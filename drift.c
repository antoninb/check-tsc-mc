/*
 * Copyright (C) 2009 Canonical
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

/*
 *  Author Colin Ian King,  colin.king@canonical.com
 */

#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>

typedef unsigned long long u64;
typedef unsigned long      u32;

struct timep{
  u64 t;
  pthread_mutex_t lock;
};

pthread_attr_t attr;

#define SIZE 5
#define NUM_THREADS 4
#define PRINT_LIMIT 100

struct timep array[SIZE];
pthread_t callThd[NUM_THREADS];

static inline u64 rdtsc(void)
{
  if (sizeof(long) == sizeof(u64)) {
    u32 lo, hi;
    asm volatile("rdtsc" : "=a" (lo), "=d" (hi));
    return ((u64)(hi) << 32) | lo;
  }
  else {
    u64 tsc;
    asm volatile("rdtsc" : "=A" (tsc));
    return tsc;
  }
}

void *doTask(void* arg){
  u64 tsc;
  u64 cnt = 0L;
  //long long diff;
  while(1){
    int j = rand() % SIZE;
    pthread_mutex_lock(&array[j].lock);
    tsc = rdtsc();
    if(tsc < array[j].t){
      cnt++;
      if(array[j].t - tsc > PRINT_LIMIT)
	printf("bad with diff %lld\n", tsc - array[j].t);
    }
    array[j].t = tsc;
    pthread_mutex_unlock(&array[j].lock);
  }
}

int main(int argc, char **argv)
{
  int i;
  void *status;
  for(i = 0; i < SIZE; i++){
    array[i].t = rdtsc();
    pthread_mutex_init(&(array[i].lock), NULL);
  }
  
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  for(i = 0; i < NUM_THREADS; i++){
    pthread_create(&callThd[i], &attr, doTask, NULL);
  }

  pthread_attr_destroy(&attr);

  for(i = 0; i < NUM_THREADS; i++){
    pthread_join(callThd[i], &status);
  }

  exit(EXIT_SUCCESS);
}
