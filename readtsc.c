/*
 * Copyright (C) 2009 Canonical
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

/*
 *  Author Colin Ian King,  colin.king@canonical.com
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sched.h>

typedef unsigned long long u64;
typedef unsigned long      u32;

static inline u64 rdtsc(void)
{
  if (sizeof(long) == sizeof(u64)) {
    u32 lo, hi;
    asm volatile("rdtsc" : "=a" (lo), "=d" (hi));
    return ((u64)(hi) << 32) | lo;
  }
  else {
    u64 tsc;
    asm volatile("rdtsc" : "=A" (tsc));
    return tsc;
  }
}

int main(int argc, char **argv)
{
  //printf("%llx\n",rdtsc());
  u64 tsc1, tsc2;
  tsc1 = 0;
  tsc2 = 1;
  int cpu1, cpu2;
  cpu_set_t mask;
  int c = 0;
  CPU_ZERO(&mask);
  CPU_SET(c, &mask);
  sched_setaffinity(0, sizeof(mask), &mask);
  while(tsc2 >= tsc1){
    cpu1 = sched_getcpu();
    tsc1 = rdtsc();
    //sleep(0);

    c = rand() % 4;
    CPU_ZERO(&mask);
    CPU_SET(c, &mask);
    sched_setaffinity(0, sizeof(mask), &mask);
    
    cpu2 = sched_getcpu();
    tsc2 = rdtsc();
    if(cpu2 != cpu1 && tsc2 >= tsc1)
      printf("different cpus %d %d and yet consistent %llx\n", cpu1, cpu2, tsc2 - tsc1);
  }
  exit(EXIT_SUCCESS);
}
