CFLAGS += -O2
LIBS += -lpthread

readtsc: readtsc.o

drift: drift.o
	gcc -O2 -o drift drift.o -lpthread 

all: readtsc drift

clean:
	rm -f readtsc readtsc.o drift drift.o
